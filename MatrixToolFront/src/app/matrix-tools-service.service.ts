import { Injectable } from '@angular/core';
import { MatrixData } from './matrix-data/Model/MatrixData';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MatrixToolsService {

	private matrixToolApiUrl = "http://localhost:3000/api/matrix/tools";
	private determinantOpSuffix = "/operations/determinant";
	
  constructor(private http:HttpClient) { }
  
  // Loads the matrix data in order to initialize the default 2X2 matrix
  loadMatrixData() : Observable<MatrixData>{
	  return of(new MatrixData());
  }
  
  // Resizes the given matrix according to the MatrixData.size of that instance
  ResizeMatrix(matrixData : MatrixData): Observable<MatrixData>{
	  matrixData.ResizeMatrix();
	  return of(matrixData);
  }
  
  // Calls the API in order to delegate the determinant computation of the given matrix.
  // The computed determinant will be stored in the MatrixData.determinant
  ComputeDeterminant(matrixData : MatrixData): Observable<any | MatrixData>{
	  //matrixData.ComputeDeterminant();
	  //return of(matrixData);
	return this.http.post<any | MatrixData>(this.matrixToolApiUrl + this.determinantOpSuffix, matrixData, httpOptions)
		.pipe(
			catchError(this.handleError('computeDeterminant', []))
		);
  }
  
	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T> (operation = 'operation', result?: T) {
	  return (error: any): Observable<T> => {
	 
		// TODO: send the error to remote logging infrastructure
		console.error(error); // log to console instead	 
	 
		// Let the app keep running by returning an empty result.
		return of(result as T);
	  };
	}
}
