import { TestBed, inject } from '@angular/core/testing';

import { MatrixToolsService } from './matrix-tools-service.service';

describe('MatrixToolsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatrixToolsService]
    });
  });

  it('should be created', inject([MatrixToolsService], (service: MatrixToolsService) => {
    expect(service).toBeTruthy();
  }));
});
