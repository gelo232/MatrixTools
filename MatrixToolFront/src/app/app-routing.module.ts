import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatrixDataComponent } from './matrix-data/matrix-data.component';

const routes: Routes = [
	{ path: 'matrix/tools', component: MatrixDataComponent },
	{ path: '', redirectTo: '/matrix/tools', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
	
}
