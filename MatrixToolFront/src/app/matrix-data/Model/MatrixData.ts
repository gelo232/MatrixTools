
export class MatrixData{
	
  public minSize : number = 2;
  public maxSize : number = 8;
  public size : number = 2;
  public matrix : Array<number> = [];
  public determinant : number = 0;
  
  // Creates a square matrix of matrixSize X matrixSize
  private createMatrix(matrixSize: number):Array<number>{
	let res : Array<number> = [];
	for(var i = 0; i < matrixSize; i++){
		for(var j = 0; j < matrixSize; j++)
			res[i*matrixSize + j] = 0;
	}
	return res;
  }
  
  constructor() { 
	this.matrix = this.createMatrix(this.size);
  }
  indexTracker(index: number, value: any) {
    return index;
  }
  
  // Parses the src object into this MatrixData instance by retrieving the related properties
  PopulateFrom(src:any):void{
	  this.minSize = src["minSize"];
	  this.maxSize = src["maxSize"];
	  this.size = src["size"];
	  var mats = src["matrix"];
	  for(var i = 0; i < mats.length; i++)
		  this.matrix[i] = parseInt(mats[i]);
	  
	  this.determinant = src["determinant"];
	  //console.log(src);
	  //console.log(this);
  }
  
  // Resizes the matrix so that it matches the MatrixData.size
  ResizeMatrix():void{
	  let tmpMatrix: Array<number> = this.createMatrix(this.size);
	  var oldSize = Math.sqrt(this.matrix.length);
	  var newSize = this.size;
	  
	  var dataSize = Math.min(tmpMatrix.length, this.matrix.length);
	  var dataSideSize = Math.sqrt(dataSize);
	  
	  for(var i = 0; i < dataSideSize; i++)
		  for(var j = 0; j < dataSideSize; j++)
			tmpMatrix[i*newSize + j] = this.matrix[i*oldSize + j];
	  this.matrix = tmpMatrix; 
  } 

}