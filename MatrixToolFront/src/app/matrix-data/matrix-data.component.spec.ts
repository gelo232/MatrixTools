import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatrixDataComponent } from './matrix-data.component';

describe('MatrixDataComponent', () => {
  let component: MatrixDataComponent;
  let fixture: ComponentFixture<MatrixDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatrixDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatrixDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
