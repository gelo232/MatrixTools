import { Component, OnInit } from '@angular/core';
import { MatrixData } from './Model/MatrixData';
import { MatrixToolsService } from '../matrix-tools-service.service';

@Component({
  selector: 'app-matrix-data',
  templateUrl: './matrix-data.component.html',
  styleUrls: ['./matrix-data.component.css']
})
export class MatrixDataComponent implements OnInit {
  
  matrixData: MatrixData;
  
  constructor(private matrixService:MatrixToolsService) { 
  }
  
  // Calls the MatrixService in order to load a MatrixData
  loadMatrixData():void{
	  this.matrixService.loadMatrixData()
		.subscribe(matrixData => this.matrixData = matrixData);
  }

  ngOnInit() {
	  this.loadMatrixData();
  }
  
  // Calls the Matrix Service in order to resize the matrix of the current loaded MatrixData
  ResizeMatrix(){
	  this.matrixService.ResizeMatrix(this.matrixData)
		.subscribe();
  }
  
  // Calls the Matrix Service in order to perform the determinant computation
  ComputeDeterminant(){
	  this.matrixService.ComputeDeterminant(this.matrixData)
		.subscribe(matrix => {
			let m:MatrixData = new MatrixData();
			try{				
				m.PopulateFrom(matrix);
			}catch (e) {
				console.log((<Error>e));
			}
			this.matrixData = m;

		});
  }
  
  // Used to generat an array of numbers for looping purpose in the view side
  Range(a:number, b:number):Array<number>{
	  let rg:Array<number> = [];
	  for(var i = a; i <= b; i++)
		  rg.push(i);
	  return rg;
  }
}
