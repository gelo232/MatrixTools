
recurOperator = {
	createMatrix : function(matrixSize){
		var res = [];
		for(var i = 0; i < matrixSize; i++){
			for(var j = 0; j < matrixSize; j++)
				res[i*matrixSize + j] = 0;
		}
		return res;
	},
  
  // Extracts the matrix obtained by removing the subRow'nth row and the subCol'nth column from the defaultMatrix
	extractMinorMatrix:function(subRow, subCol, defaultMatrix){
		  var defaultSize = Math.sqrt(defaultMatrix.length);
		  var minorMatrix = this.createMatrix(defaultSize - 1);
		  var defRow = 0;
		  var mRow = 0;
		  while(defRow < defaultSize){
			if( defRow == subRow){
				defRow++;
				continue;
			}
			var defCol = 0;
			var mCol = 0;
			while(defCol < defaultSize){
				if( defCol == subCol){
					defCol++;
					continue;
				}
				
				minorMatrix[mRow * (defaultSize - 1) + mCol] = defaultMatrix[defRow * defaultSize + defCol];
				
				defCol++;
				mCol++;
			}
			defRow++;
			mRow++;
		}
		
		return minorMatrix;
		  
	},
	
	// Computes the determinant of the minor matrix coming from defaultMatrix (see extractMinorMatrix comments for more info about the minor matrix)
	computeMinor : function(subRow, subCol, defaultMatrix){	  
		var minorMatrix = this.extractMinorMatrix(subRow, subCol, defaultMatrix);
		var minorSize = Math.sqrt(minorMatrix.length);
		
		if(minorSize == 1)
			return minorMatrix[0];
		
		var i = 0;
		var d = 0;
		for(var j = 0; j < minorSize; j++){
			d += Math.pow(-1, (i+1) + (j+1)) * minorMatrix[i * minorSize + j] * this.computeMinor(i, j, minorMatrix);
		}
		
		return d;
	}
}

exports.ComputeDeterminant = function(matrix){
	if(!(matrix instanceof Array)){
		return{
			type: "Error",
			message: "Matrix type error: must be an array"
		};
	}
	
	var size = Math.sqrt(matrix.length);
	var i = 0;
	var determinant = 0;
	for(var j = 0; j < size; j++){
		determinant += Math.pow(-1, (i+1) + (j+1)) * matrix[i*size + j] * recurOperator.computeMinor(i, j, matrix);
	}
	
	return determinant;
};
