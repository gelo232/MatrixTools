var express = require('express');
var router = express.Router();
var matrixOps = require('../lib/MatrixOperation');

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/* Performs the determinant computation operation. */
router.post('/matrix/tools/operations/determinant', function(req, res, next) {	
  mDat = req.body;
  var matrix = mDat.matrix;
  mDat.determinant = matrixOps.ComputeDeterminant(matrix);
  
  res.json(mDat);
});

module.exports = router;
